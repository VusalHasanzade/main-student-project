package az.ingress.ms14.dto;

import jakarta.persistence.Entity;
import lombok.Data;

@Data
public class StudentDto {
    Integer id;
    String name;
    String surname;
}
