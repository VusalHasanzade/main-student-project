package az.ingress.ms14.dto;

import lombok.Data;

@Data
public class CreateStudentDto {

    String name;
    String surname;
}
