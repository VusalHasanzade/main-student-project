package az.ingress.ms14.service;

import az.ingress.ms14.dto.CreateStudentDto;
import az.ingress.ms14.dto.StudentDto;
import az.ingress.ms14.dto.UpdateStudentDto;
import az.ingress.ms14.model.Student;
import az.ingress.ms14.repository.StudentRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StudentService {
    private final StudentRepository studentRepository;
    private final ModelMapper modelMapper;

    public StudentService(StudentRepository studentRepository, ModelMapper modelMapper) {
        this.studentRepository = studentRepository;
        this.modelMapper = modelMapper;
    }

    public void create(CreateStudentDto dto) {
       // var student = new Student();
       // student.setName(dto.getName());
       // student.setSurname(dto.getSurname());

        Student student = modelMapper.map(dto, Student.class);
        studentRepository.save(student);
    }

    public void update(UpdateStudentDto dto) {
        Optional<Student> entity = studentRepository.findById(dto.getId());
        entity.ifPresent(student1 -> {
            student1.setName(dto.getName());
            student1.setSurname(dto.getSurname());
        studentRepository.save(student1);
        });
    }

    public StudentDto get(Integer id) {
        Student student = studentRepository.findById(id).get();
        //StudentDto studentDto = new StudentDto();
        //studentDto.setId(student.getId());
        //studentDto.setName(student.getName());
        //studentDto.setSurname(student.getSurname());

        StudentDto studentDto = modelMapper.map(student, StudentDto.class);
        return studentDto;
    }

    public void delete(Integer id) {
        studentRepository.deleteById(id);
    }

    public List<Student> getAll(){
        return studentRepository.findAll();
    }

}
