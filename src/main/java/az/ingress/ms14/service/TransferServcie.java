package az.ingress.ms14.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class TransferServcie {

    private String AZERICARD_URL;

    @Value("${azericard.url}")
    public void transfer() {
        System.out.println("go to " + AZERICARD_URL + " and transfer some amount of money");
    }
}
