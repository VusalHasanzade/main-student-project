package az.ingress.ms14;

import az.ingress.ms14.configuration.AppConfig;
import az.ingress.ms14.model.Student;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;

@Configuration
@SpringBootApplication
@RequiredArgsConstructor
public class Ms14Application implements CommandLineRunner {

	//@Value("${ms.name}")
	//private String name;

	//@Value("${ms.teacher}")
	//private String teacher;

	private final AppConfig appConfig;

	public static void main(String[] args) {
		SpringApplication.run(Ms14Application.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println(appConfig.getName());
		System.out.println(appConfig.getTeacher());
		System.out.println(appConfig.getStudents());
	}
}
